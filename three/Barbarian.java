public class Barbarian extends Character {

    public Barbarian(String name) {
        this.name = name;
        this.level = 1;
        this.baseHitpoints = 1200;
        this.baseManapoints = 300;
        this.baseAgility = 5;
        this.baseStrength = 20;
        this.baseIntelligence = 5;
    }

    public String dropLeftWeapon(){
        this.leftHandWeapon = null;
        return this.leftHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setLeftWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.leftHandWeapon == null){
            if (weapon instanceof Broadsword){
                this.leftHandWeapon = weapon;
            }else if(weapon instanceof Pike && (this.rightHandWeapon == null || !(this.rightHandWeapon instanceof Pike))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }else if(weapon instanceof Spear && (this.rightHandWeapon == null || !(this.rightHandWeapon instanceof Spear))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }
            else{
                retMsg = "Sorry, "+this.getClass().getSimpleName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

    public String dropRightWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setRightWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.rightHandWeapon == null){
            if (weapon instanceof Broadsword){
                this.rightHandWeapon = weapon;
            }else if(weapon instanceof Pike && (this.leftHandWeapon == null || !(this.leftHandWeapon instanceof Pike))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }else if(weapon instanceof Spear && (this.leftHandWeapon == null || !(this.leftHandWeapon instanceof Spear))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }
            else{
                retMsg = "Sorry, "+this.getName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

}
