public abstract class Character {

    protected String name;
    protected int level;

    protected int baseHitpoints;
    protected int baseManapoints;
    protected int baseEvasions = 500;

    protected int baseAgility;
    protected int baseStrength;
    protected int baseIntelligence;

    protected Weapon leftHandWeapon;
    protected Weapon rightHandWeapon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void levelUp(){
        this.level += 1;
        this.baseHitpoints += this.baseStrength * this.level;
        this.baseManapoints += this.baseIntelligence * this.level;
        this.baseEvasions += this.baseAgility * this.level;
    }

    public abstract void dropRightWeapon() {
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public abstract void dropLeftWeapon(){
        this.leftHandWeapon = null;
        return this.leftHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() + " Lv." + this.getLevel() + "-" + this.getName() ;
    }

}
