public class Archer extends Character {

    public Archer(String name) {
        this.name = name;
        this.level = 1;
        this.baseHitpoints = 900;
        this.baseManapoints = 600;
        this.baseAgility = 15;
        this.baseStrength = 10;
        this.baseIntelligence = 5;
    }

    public String dropLeftWeapon(){
        this.leftHandWeapon = null;
        return this.rightLeftWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setLeftWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.leftHandWeapon == null){
            if(weapon instanceof Bow && !(this.rightHandWeapon == null || !(this.rightHandWeapon instanceof Bow))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getClass().getSimpleName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

    public String dropRightWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setRightWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.rightHandWeapon == null){
            if(weapon instanceof Bow && (this.leftHandWeapon == null || !(this.leftHandWeapon instanceof Bow))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }
}
