public class Knight extends Character {

    public Knight(String name) {
        this.name = name;
        this.level = 1;
        this.baseHitpoints = 750;
        this.baseManapoints = 750;
        this.baseAgility = 10;
        this.baseStrength = 10;
        this.baseIntelligence = 10;
    }

    public String dropLeftWeapon(){
        this.leftHandWeapon = null;
        return this.leftHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setLeftWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.leftHandWeapon == null){
            if (weapon instanceof Broadsword){
                this.leftHandWeapon = weapon;
            } else if (weapon instanceof Shield && !(this.rightHandWeapon instanceof Shield)){
                this.leftHandWeapon = weapon;
            }
            else{
                retMsg = "Sorry, "+this.getClass().getSimpleName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }

        return retMsg;
    }

    public String dropRightWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setRightWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.rightHandWeapon == null){
            if (weapon instanceof Broadsword){
                this.rightHandWeapon = weapon;
            } else if (weapon instanceof Shield && !(this.leftHandWeapon instanceof Shield)){
                this.rightHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

}
