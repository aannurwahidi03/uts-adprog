public class Mage extends Character {

    public Mage(String name) {
        this.name = name;
        this.level = 1;
        this.baseHitpoints = 600;
        this.baseManapoints = 1200;
        this.baseAgility = 5;
        this.baseStrength = 5;
        this.baseIntelligence = 20;
    }

    public String dropLeftWeapon(){
        this.leftHandWeapon = null;
        return this.leftHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setLeftWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.leftHandWeapon == null){
            if(weapon instanceof WizardStaff && (this.rightHandWeapon == null || !(this.rightHandWeapon instanceof WizardStaff))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getClass().getSimpleName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

    public String dropRightWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setRightWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.rightHandWeapon == null){
            if(weapon instanceof WizardStaff && (this.leftHandWeapon == null || !(this.leftHandWeapon instanceof WizardStaff))){
                this.leftHandWeapon = weapon;
                this.rightHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

}
