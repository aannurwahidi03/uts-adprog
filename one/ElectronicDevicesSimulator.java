public class ElectronicDevicesSimulator{
    public static void main(String[] args){
        //Create all component of SmartTV
        PowerSupply powerSupply = new PowerSupply();
        Motherboard motherboard = new Motherboard();
        LCDScreen lcdScreen = new LCDScreen();
        SmartTV smartTV = new SmartTV(lcdScreen, motherboard, powerSupply);

        smartTV.switchOn();

        smartTV.switchOff();
    }
}

class SmartTV {

    private LCDScreen lcdScreen;
    private Motherboard motherboard;
    private PowerSupply powerSupply;

    public SmartTV(LCDScreen lcdScreen, Motherboard motherboard, PowerSupply powerSupply){
        this.lcdScreen = lcdScreen;
        this.motherboard = motherboard;
        this.powerSupply = powerSupply;
    }

    public void switchOn() {
        //Turn all component of SmartTV on to switch the TV on
        powerSupply.switchOn();
        motherboard.switchOn();
        lcdScreen.switchOn();
        System.out.println("Smart TV is On");
    }

    public void switchOff()
    {
        //Turn all component of SmartTV off to switch the TV off
        System.out.println("Smart TV is Off");
        lcdScreen.switchOff();
        motherboard.switchOff();
        powerSupply.switchOff();
    }

}
class LCDScreen {
    public void switchOn() {
        System.out.println("LCD Screen is On");
    }

    public void switchOff() {
        System.out.println("LCD Screen is Off");
    }
}

class PowerSupply {
    public void switchOn() {
        System.out.println("Power Supply is On");
    }

    public void switchOff() {
        System.out.println("Power Supply is Off");
    }
}

class Motherboard {
    public void switchOn() {
        System.out.println("Motherboard is On");
    }

    public void switchOff() {
        System.out.println("Motherboard is Off");
    }
}

